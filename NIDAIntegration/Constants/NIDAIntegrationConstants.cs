﻿namespace NIDAIntegration.Constants
{
    public class NIDAIntegrationConstants
    {
        // The application key to the service provider's certificate.
        public const string Spx509Certificate = "spX509Certificate";

        // The application key to the identity provider's certificate.
        public const string IdPx509Certificate = "idpX509Certificate";
    }
}