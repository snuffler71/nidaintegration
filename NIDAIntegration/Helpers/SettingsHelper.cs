﻿using NIDAIntegration.Properties;

namespace NIDAIntegration.Helpers
{
    public class SettingsHelper
    {
        public static string GetAccountManagementUrl()
        {
            return Settings.Default.IdentityProviderAccountManagementUrl;
        }
    }
}