﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace NIDAIntegration.Helpers
{
    public class TokenFactory
    {
        // Create a SAS token. SAS tokens are described in http://msdn.microsoft.com/en-us/library/windowsazure/dn170477.aspx.
        public static string GetSasToken(string serviceNamespace, string sasKeyName, string sasKey)
        {
            // Set token lifetime to 480 minutes.
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = DateTime.Now.ToUniversalTime() - origin;
            uint tokenExpirationTime = Convert.ToUInt32(diff.TotalSeconds) + 480 * 60;

            string stringToSign = HttpUtility.UrlEncode(serviceNamespace) + "\n" + tokenExpirationTime;
            HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(sasKey));

            string signature = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign)));
            string token = String.Format(CultureInfo.InvariantCulture, "SharedAccessSignature sr={0}&sig={1}&se={2}&skn={3}",
                HttpUtility.UrlEncode(serviceNamespace), HttpUtility.UrlEncode(signature), tokenExpirationTime, sasKeyName);
            Console.WriteLine("Token: " + token);
            return token;
        }
    }
}
