﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NIDAIntegration.Helpers
{
    public class RelayModel
    {
        public string ServiceNamespace { get; set; }
        public string SasKeyName { get; set; }
        public string SasKey { get; set; }
        public string Payload { get; set; }
        public string Method { get; set; }
    }
}
