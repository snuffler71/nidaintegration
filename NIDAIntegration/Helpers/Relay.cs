﻿using Nida.ServiceProvider.Dto.Response;
using NIDAIntegration.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DARDMeatInspection.API.Helpers
{
    public class Relay
    {
        public static HttpResponseMessage Post(RelayModel model)
        {
            //string baseAddress = string.Format("http://{0}.servicebus.windows.net/", model.ServiceNamespace);
            string serviceAddress = string.Format("https://{0}.servicebus.windows.net/api/{1}", model.ServiceNamespace, model.Method);

            var httpClient = new HttpClient();
            var token = TokenFactory.GetSasToken(model.ServiceNamespace, model.SasKeyName, model.SasKey);

            httpClient.DefaultRequestHeaders.Add("Authorization", token);
            httpClient.DefaultRequestHeaders.Add("ContentType", "application/json");

            HttpResponseMessage result = httpClient.PostAsync(serviceAddress, 
                new StringContent(model.Payload, Encoding.UTF8, "application/json")).Result;

            return result;

        }

        public static string Get(RelayModel model)
        {
            string baseAddress = string.Format("http://{0}.servicebus.windows.net/", model.ServiceNamespace);
            string serviceAddress = string.Format("https://{0}.servicebus.windows.net/api/{1}/{2}", model.ServiceNamespace, model.Method, model.Payload);

            var httpClient = new HttpClient();
            var token = TokenFactory.GetSasToken(model.ServiceNamespace, model.SasKeyName, model.SasKey);

            httpClient.DefaultRequestHeaders.Add("Authorization", token);
            httpClient.DefaultRequestHeaders.Add("ContentType", "application/json");

            var result = httpClient.GetStringAsync(serviceAddress).Result;

            return result;

        }
    }
}
