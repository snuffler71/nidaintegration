﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace NIDAIntegration.Helpers
{
    public static class ConfigurationHelper
    {
        public static string SasKeyName { get { return ConfigurationManager.AppSettings["SasKeyName"]; } }
        public static string ServiceNamespace { get { return ConfigurationManager.AppSettings["ServiceNamespace"]; } }
        public static string SasKey { get { return ConfigurationManager.AppSettings["SasKey"]; } }
        public static Boolean IsProduction { get { return Boolean.Parse(ConfigurationManager.AppSettings["IsProduction"]); } }

    }
}