﻿using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using Nida.Saml.ServiceProvider;
using Nida.Constants;
using NIDAIntegration.Properties;
using NidaIntegration.Saml;
using NIDAIntegration.Constants;
using System.Collections.Generic;
using NIDAIntegration.Models;
using System;
using NIDAIntegration.CISService;
using NIDAIntegration.Helpers;

namespace NIDAIntegration.Controllers
{
    public class SamlController : Controller
    {
        private RelayModel _relayModel;

        public SamlController()
        {

            _relayModel = new RelayModel
            {
                SasKeyName = ConfigurationHelper.SasKeyName,
                ServiceNamespace = ConfigurationHelper.ServiceNamespace,
                SasKey = ConfigurationHelper.SasKey,
                Method = "OnlineUser"
            };
        }
        public ActionResult AssertionConsumerService()
        {
            // Load Idp public certificate
            var identityProviderX509Certificate = (X509Certificate2)((WebApiApplication)HttpContext.ApplicationInstance).Application[NIDAIntegrationConstants.IdPx509Certificate];

            // Load Sp certificate + private key
            //var fileName = Path.Combine(HttpRuntime.AppDomainAppPath, Settings.Default.SpCertificateFileName);
            var fileName = HttpContext.Server.MapPath(String.Format("~/App_Data/{0}", Settings.Default.SpCertificateFileName));
            var serviceProviderX509Certificate = new X509Certificate2(fileName, Settings.Default.SpPassword);

            // set up out variables
            string persistentId;
            int levelOfAssurance;
            bool levelOfAssurancePending;
            bool success;
            string message;
            string inResponseToSamlRequestId;

            var samlAssertionReceiver = new SamlAssertionReceiver(identityProviderX509Certificate, serviceProviderX509Certificate);
            samlAssertionReceiver.ProcessSamlResponse(out persistentId, out inResponseToSamlRequestId, out levelOfAssurance, out levelOfAssurancePending, out success, out message);

            //var cisService = new CISServiceClient();

            //Guid persistentGuid;
            //if (!Guid.TryParse(persistentId, out persistentGuid))
            //{
            //    // error with Parsing Saml
            //}

            //var onlineUser = cisService.GetOnlineUser(persistentGuid);

            var model = new SamlResponseViewModel
            {
                PersistentId = persistentId,
                InResponseToSamlRequestId = inResponseToSamlRequestId,
                LevelOfAssurance = levelOfAssurance,
                LevelOfAssurancePending = levelOfAssurancePending,
                Success = success,
                Message = message,
                OnlineUser = new OnlineUser()
            };

            return View(model);
        }

        public ActionResult SendSamlRequest(int levelOfAssurance, string persistentId = "")
        {
            var serviceProviderX509Certificate2 = (X509Certificate2)((WebApiApplication)HttpContext.ApplicationInstance).Application[NIDAIntegrationConstants.Spx509Certificate];
            var identityProviderX509Certificate2 = (X509Certificate2)((WebApiApplication)HttpContext.ApplicationInstance).Application[NIDAIntegrationConstants.IdPx509Certificate];
            string samlRequestId;

            var samlSender = new SamlRequestSender(Settings.Default.ServiceProviderUrl, Settings.Default.IdentityProviderRequestConsumerUrl, serviceProviderX509Certificate2, identityProviderX509Certificate2);
            samlSender.SendAuthenticationRequest(levelOfAssurance, out samlRequestId, persistentId);

            System.Web.HttpContext.Current.Response.End();
            return null;
        }

        public ActionResult MatchRequests()
        {
            var filenames = Directory.EnumerateFiles(Settings.Default.MatchRequestsFolder);

            var jsonModel = new JSONModel { MatchRequests = new List<string>() };
            foreach (var filename in filenames)
            {
                jsonModel.MatchRequests.Add(System.IO.File.ReadAllText(filename));

            }

            return View(jsonModel);
        }
    }
}