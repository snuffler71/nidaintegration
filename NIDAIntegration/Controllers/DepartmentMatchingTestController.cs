﻿using log4net;
using Newtonsoft.Json;
using Nida.Constants;
using Nida.ServiceProvider.Dto.Request;
using Nida.ServiceProvider.Dto.Response;
using NIDAIntegration.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using FuzzyString;
using NIDAIntegration.Helpers;
using DARDMeatInspection.API.Helpers;
using System.Web;

namespace NIDAIntegration.Controllers
{
    [RoutePrefix("api/department-match")]
    public class DepartmentMatchingTestController : ApiController
    {
        private RelayModel _relayModel;
        private ILog _logger;

        public DepartmentMatchingTestController()
        {
            _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

            _relayModel = new RelayModel
            {
                SasKeyName = ConfigurationHelper.SasKeyName,
                ServiceNamespace = ConfigurationHelper.ServiceNamespace,
                SasKey = ConfigurationHelper.SasKey,
                Method = "Match"
            };
        }

        [Route("")]
        public HttpResponseMessage Post([FromBody]ServiceMatchingDataSetRequest matchingDataDetails)
        {

            //// If the name passed down as part of the MDS matches the configured name in settings a
            //// nomatch value is returned.
            MatchingDataSetResponse matchingDataSetResponse = new MatchingDataSetResponse();
            matchingDataSetResponse.Result = MatchingDataSetConstants.Match;

            //matchingDataDetails.FirstName.Value.ApproximatelyEquals("John", )

            //_logger.DebugFormat("CLASS: {0} Recieved MDS for PersistentId[{1}] - RequestId: {2} - MatchResult: {3} ", this.GetType(), matchingDataDetails.persistentId, matchingDataDetails.requestId, matchingDataSetResponse.Result);

            //if (Settings.Default.SaveServiceMatchingDataSetRequestToFile)
            //{
            //    SaveServiceMatchingDataSetRequestToFile(matchingDataDetails);
            //}

            _relayModel.Payload = JsonConvert.SerializeObject(matchingDataDetails);

            try
            {
                var result = Relay.Post(_relayModel);
                //return Request.CreateResponse(HttpStatusCode.OK, result.Content.ReadAsAsync<MatchingDataSetResponse>().Result);
                var stringResult = result.Content.ReadAsStringAsync().Result;
                var response = JsonConvert.DeserializeObject<MatchingDataSetResponse>(stringResult);
                return Request.CreateResponse(HttpStatusCode.OK, response);
                
            }
            catch(Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return Request.CreateResponse(HttpStatusCode.OK, matchingDataSetResponse);
            }

            //var nidaService = new NIDAService.NIDAServiceClient();
            //var nidaResponse = nidaService.Match(matchingDataDetails);
            //return Request.CreateResponse(HttpStatusCode.OK, nidaResponse);
           
        }

        private void SaveServiceMatchingDataSetRequestToFile(ServiceMatchingDataSetRequest serviceMatchingDataSetRequest)
        {
            var json = JsonConvert.SerializeObject(serviceMatchingDataSetRequest);

            var folder = Settings.Default.MatchRequestsFolder;

            if (!System.IO.Directory.Exists(folder)) System.IO.Directory.CreateDirectory(folder);

            System.IO.File.WriteAllText(string.Format(@"{0}\{1:yyyy-MM-dd HH-mm-ss}.json", folder, DateTime.Now), json);
        }
    }
}
