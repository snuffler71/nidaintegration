﻿using log4net.Config;
using NIDAIntegration.App_Start;
using NIDAIntegration.Constants;
using NIDAIntegration.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NIDAIntegration
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //GlobalConfiguration.Configuration.Filters.Add(new GeneralExceptionFilter());

            XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));

            // Load the SP certificate
            //string fileName = Path.Combine(HttpRuntime.AppDomainAppPath, Settings.Default.SpCertificateFileName);
            string fileName = HttpContext.Current.Server.MapPath(String.Format("~/App_Data/{0}", Settings.Default.SpCertificateFileName));
            Application[NIDAIntegrationConstants.Spx509Certificate] = LoadCertificate(fileName, Settings.Default.SpPassword);

            // Load the IdP certificate
            fileName = HttpContext.Current.Server.MapPath(String.Format("~/App_Data/{0}", Settings.Default.IdpCertificateFileName));
            //Path.Combine(HttpRuntime.AppDomainAppPath, Settings.Default.IdpCertificateFileName);
            Application[NIDAIntegrationConstants.IdPx509Certificate] = LoadCertificate(fileName, null);

            //var ex = new Exception(String.Format("{0} {1}", Application[NIDAIntegrationConstants.Spx509Certificate].ToString(),
            //    Application[NIDAIntegrationConstants.IdPx509Certificate].ToString()));

            //Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
        }

        // Loads the certificate from file. A password is only required if the file contains a
        // private key. The machine key set is specified so the certificate is accessible to the IIS process.
        private X509Certificate2 LoadCertificate(string fileName, string password)
        {
            X509Certificate2 functionReturnValue = default(X509Certificate2);

            if (!File.Exists(fileName))
            {
                throw new ArgumentException("The certificate file " + fileName + " doesn't exist.");
            }

            try
            {
                functionReturnValue = new X509Certificate2(fileName, password, X509KeyStorageFlags.MachineKeySet);
            }
            catch (Exception exception)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(exception));
                throw new ArgumentException("The certificate file " + fileName + " couldn't be loaded - " + exception.Message, exception);
            }

            return functionReturnValue;
        }
    }
}
