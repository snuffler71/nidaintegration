﻿using System;
using System.Web;
using System.Web.Optimization;

namespace NIDAIntegration
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            if (bundles == null)
            {
                throw new ArgumentNullException("bundles");
            }

            // NICS UXM with Bootstrap2
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.min.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/nidirect.css",
                        "~/Content/nidirect-responsive.css",
                        "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                "~/Scripts/jquery-{version}.js"));
        }
    }
}
