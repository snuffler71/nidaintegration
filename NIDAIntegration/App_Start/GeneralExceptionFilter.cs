﻿using log4net;
using Logging.CMBP.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http.Filters;

namespace NIDAIntegration.App_Start
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(context.Exception));
        }
    }

    public class GeneralExceptionFilter : ExceptionFilterAttribute
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null)
            {
                var ex = actionExecutedContext.Exception;
                var request = actionExecutedContext.Request;

                // Create a Guid reference for the error so we can find this exception in the log
                var guid = Guid.NewGuid();

                // Log the exception details, including the uri that was requested and the reference
                Logger.ErrorFormat("Error - ref: {0} - uri: {1} - message: {2}", guid, request.RequestUri, ex.ExtendedMessage());

                // Send a friendly response to the client, including the reference for the exception
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError,
                    string.Format("An unexpected error occurred on the server. Please check the logs for details. Ref: {0}", guid));
            }
            else
            {
                base.OnException(actionExecutedContext);
            }
        }
    }
}