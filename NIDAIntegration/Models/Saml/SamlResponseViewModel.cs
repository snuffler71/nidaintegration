﻿using NIDAIntegration.CISService;

namespace NidaIntegration.Saml
{
    public class SamlResponseViewModel
    {
        public string PersistentId { get; set; }

        public string InResponseToSamlRequestId { get; set; }

        public int LevelOfAssurance { get; set; }

        public bool LevelOfAssurancePending { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public OnlineUser OnlineUser { get; set; }
    }
}